let (let/) = Lwt.bind
let unit = Lwt.return_unit
let return = Lwt.return

let exec command args () =
  let/ _ = Lwt_process.exec (command, Array.of_list (command :: args)) in
  unit

let pread command args =
  Lwt_process.pread (command, Array.of_list (command :: args))

let pmap command args input =
  Lwt_process.pmap (command, Array.of_list (command :: args)) input

let (=~) string rex =
  let module Rex = Re.Perl in
  match Re.exec_opt (Rex.compile_pat rex) string with
    | None ->
        []
    | Some groups ->
        match Array.to_list (Re.Group.all groups) with
          | [] ->
              []
          | _ :: tail ->
              tail

let (=~*) string rex =
  let module Rex = Re.Perl in
  Re.matches (Rex.compile_pat rex) string

type entry =
  {
    title: string;
    action: unit -> unit Lwt.t;
  }

let () =
  Lwt_main.run @@

  (* Read clipboard (using xclip). *)
  let/ input =
    Lwt.catch
      (fun () -> pread "xclip" [ "-o" ])
      (function
        | End_of_file -> return ""
        | exn -> Lwt.fail exn)
  in

  (* Find interesting stuff in clipboard. *)
  let entries =
    let gather rex f = List.map f (input =~* rex) in
    List.flatten [
      (
        gather "\\b(B[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{50})\\b" @@ fun x ->
        {
          title = "View block in tzstats (Mainnet): " ^ x;
          action = exec "firefox" [ "--new-tab"; "https://tzstats.com/" ^ x ];
        };
      );
      (
        gather "\\b(B[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{50})\\b" @@ fun x ->
        {
          title = "View block in tzstats (Carthagenet): " ^ x;
          action = exec "firefox" [ "--new-tab"; "https://carthagenet.tzstats.com/" ^ x ];
        };
      );
      (
        gather "\\b(https?://[^ ]+)\\b" @@ fun x ->
        {
          title = "Open in FireFox: " ^ x;
          action = exec "firefox" [ "--new-tab"; x ];
        };
      );
      (
        gather "\\b([0-9]{3,5})\\b" @@ fun x ->
        {
          title = "Open Tezos merge request: " ^ x;
          action =
            exec "firefox"
              [ "--new-tab"; "https://gitlab.com/tezos/tezos/merge_requests/" ^ x ];
        };
      );
    ]
  in

  (* Prefix entries by their index. *)
  let titles = List.mapi (fun i entry -> Printf.sprintf "%d) %s" i entry.title) entries in

  (* Open dmenu and send entries. *)
  let/ selection = pmap "dmenu" [ "-i"; "-l"; "10" ] (String.concat "\n" titles) in

  (* Find out which entry was chosen and execute its action. *)
  match selection =~ "^([0-9]+)\\) " with
    | [ index ] ->
        let index = int_of_string index in
        (
          match List.nth_opt entries index with
            | Some entry ->
                entry.action ()
            | None ->
                unit
        )
    | _ ->
        unit
