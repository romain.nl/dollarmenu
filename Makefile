default:
	dune build dollarmenu.exe

clean:
	rm -rf _build
	rm -f dune-project

.PHONY: default
