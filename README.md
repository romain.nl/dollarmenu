# Dollarmenu

Dollarmenu reads the contents of the clipboard, extract some interesting
data from it (such as the hash of a Tezos block) and displays a menu
with actions on this data (such as: open the block in a block explorer).

## Prerequisites

- OCaml
- Lwt
- Re
- dmenu
- xclip

Try this:

```
opam install lwt re
apt-get install dmenu xclip
```

## Install

- Run `make` to compile.
- Put the executable (`_build/default/dollarmenu.exe`) in your `PATH`.
- In your Window Manager configuration, bind a shortcut to `dollarmenu`.

## Usage

Select text anywhere (in your terminal, in your browser, …) and press
the shortcut you bound in your Window Manager. A menu appears at the top.
Select one entry and press return.

Sometimes, such as in some web pages, just selecting text does not copy
it to clipboard. You have to copy the text to clipboard manually.